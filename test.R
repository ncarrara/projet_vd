library(gdata)
setwd("~/Work/semestre_9/VD")


df <- read.csv("projet_vd/csv/evolution_salaire_net_annuel_moyen.csv")
resultats<-data.frame(df)
colnames(resultats)


#---------------------------------- Les salaires ont ils toujours augmenté au fil du temps? -----------------------------------------

i<- 0
y <- c()
x <- c()

while(i <= nrow(resultats["Salaire_net_annuel_moyen_en_euros"])/3){
  x[i] <- resultats[3*i+1,"ANNEE"]
  y[i] <- resultats[3*i+1,"Salaire_net_annuel_moyen_en_euros"]
  i <- i + 1
}
png(filename="~/Work/semestre_9/VD/projet_vd/salaire_annuel_net_1950_2010.png")
plot(x, y,main = "Salaire annuel net moyen entre 1950 et 2010",type = "b",xlab = "année", ylab = "salaire moyen annuel (en euro)",col = "blue", pch = 3)
dev.off()
#---------------------------------- Quels sont les différences entre homme et femme au fil du temps ? -----------------------------------------

i<- 0
h <- c()
f <- c()
x <- c()

while(i <= nrow(resultats["Salaire_net_annuel_moyen_en_euros"])/3){
  x[i] <- resultats[3*i+1,"ANNEE"]
  h[i] <- resultats[3*i+2,"Salaire_net_annuel_moyen_en_euros"]
  f[i] <- resultats[3*i+3,"Salaire_net_annuel_moyen_en_euros"]
  i <- i + 1
}
png(filename="~/Work/semestre_9/VD/projet_vd/salaire_all_1950_2010.png")
plot(x, h,main = "Salaire annuel net moyen entre 1950 et 2010",type = "l",xlab = "année", ylab = "salaire moyen annuel (en euro)",col = "blue")
#lines(x, h)
lines(x,f,col="red")
legend("topleft",col=c("blue", "red"), lwd=2,lty=1,legend=c("homme","femme"))
#, cex=0.7, bty="n", xjust=1, seg.len=0.9)
dev.off()

#---------------------------------- Quelle est l’évolution des différences de salaire homme/femme? -----------------------------------------

i<- 0
y <- c()
x <- c()
print((resultats[1,"Salaire_net_annuel_moyen_en_euros"]))
while(i <= nrow(resultats["Salaire_net_annuel_moyen_en_euros"])/3){
  x[i] <- resultats[3*i+1,"ANNEE"]
  print(i)
  print((resultats[3*i+1,"Salaire_net_annuel_moyen_en_euros"]))
  print((resultats[3*i+2,"Salaire_net_annuel_moyen_en_euros"]))
  print((resultats[3*i+3,"Salaire_net_annuel_moyen_en_euros"]))
  print("--------")
  y[i] <- ((resultats[3*i+2,"Salaire_net_annuel_moyen_en_euros"]-resultats[3*i+3,"Salaire_net_annuel_moyen_en_euros"])/resultats[3*i+1,"Salaire_net_annuel_moyen_en_euros"])
  i <- i + 1
}
png(filename="~/Work/semestre_9/VD/projet_vd/salaire_H_F_1950_2010.png")
plot(x, y,main = "Evolution des différences de salaire homme/femme",type = "b",xlab = "année", ylab = "(H-F)/((H+F)/2)",col = "blue", pch = 3)
dev.off()
#------------------------------- histogramme, pr afficher salaire ensemble. X année, Y salaire ensemble, chaque année on a les diff secteurs d’activité --------

df <- read.csv("projet_vd/csv/salaires_nets_annuels_moyens_par_secteur_dactivite_b.csv")
secteurb <-data.frame(df)
colnames(secteurb)
secteurb <- subset(secteurb, select = c("ANNEE","nrj","Industrie.y.compris.construction","Transports","Commerce","Services"))

indextoremove <- c()
print(indextoremove)
i <- 0
while(i <= nrow(resultats["ANNEE"])/3){
  indextoremove <- c(indextoremove,3*i+2)
  indextoremove <- c(indextoremove,3*i+3)
  i <- i + 1
}

secteurb <- secteurb[-indextoremove,]

samp2 <- secteurb[,-1]
rownames(samp2) <- secteurb[,1]

colors <- c("black","red","green", "blue","cyan") # Define the colors you're using

png(filename="~/Work/semestre_9/VD/projet_vd/salaire_secteur_activite_1993_2007.png")

barplot(t(samp2), 
        beside=TRUE, 
        legend.text = attr(samp2, "dimnames")$ANNEE, 
        col = colors,
        main="Salaire par secteur d'activité de 1993 à 2007",
        xlab="année",
        ylab="salaire annuel moyen (en euro)")

legend("topleft", 
       legend = colnames(samp2), 
       #col = colors,
       fill = 1:6, ncol = 1,
       cex = 0.75)
dev.off()

#------------------------ stars --------------------------------------------------  

png(filename="~/Work/semestre_9/VD/projet_vd/salaire_secteur_activite__1993_2007_stars.png")
stars(samp2, locations = c(0,0), radius = TRUE,col.stars = TRUE, key.loc=c(0,0), main="Salaire par secteur d'activité de 1993 à 2007", lty = 1)
dev.off()

#------------------------  salaires_nets_annuels_moyens_selon_la_catégorie_socioprofessionnelle_b  --------------------
df <- read.csv("projet_vd/csv/salaires_nets_annuels_moyens_selon_la_catégorie_socioprofessionnelle_b.csv")
sociopro <-data.frame(df)
head(sociopro)
colnames(sociopro)
sociopro <- subset(sociopro, select = c("ANNEE","Cadres.supérieurs","Cadres.moyens","Employés","Ouvriers","dont.contremaîtres","Personnel.de.service"))
head(sociopro)
indextoremove <- c()

i <- 0
while(i <= nrow(sociopro["ANNEE"])/3){
  indextoremove <- c(indextoremove,3*i+2)
  indextoremove <- c(indextoremove,3*i+3)
  i <- i + 1
}

sociopro  <- sociopro [-indextoremove,]
head(sociopro)
samp3 <- sociopro[,-1]
rownames(samp3) <- sociopro[,1]
samp3 
sociopro

colors <- c("green","darkblue","yellow", "red","blue") # Define the colors you're using
  

png(filename="~/Work/semestre_9/VD/projet_vd/salaires_nets_annuels_moyens_selon_la_catégorie_socioprofessionnelle.png")
#par(oma=c(3,3,3,3))
#par(mar=c(20,4,4,2) + 0.9)   
stars(samp3, locations = c(0,0), radius = TRUE,col.stars = TRUE, key.loc=c(0,0), main="salaires annuels selon la catégorie socioprofessionnelle\n de 1950 à 1988 ", lty = 1)
dev.off()


#------------------------- region --------------------
df <- read.csv("projet_vd/csv/region_1966.csv")
region <-data.frame(df)
region <- sort(region)
region
region <- t(region)

#colors <- c("green","darkblue","yellow", "red","blue") # Define the colors you're using

png(filename="~/Work/semestre_9/VD/projet_vd/salaire_region_1966.png")
par(mar=c(10.5, 4, 4, 2) + 0.1)
barplot(region[,1],beside=TRUE, col=c("red","blue"),las = 2,main="Salaire par region en 1966",ylab="salaire annuel moyen (en euro)")
dev.off()

df <- read.csv("projet_vd/csv/region_2010.csv")
region <-data.frame(df)
region <- sort(region)
region
region <- t(region)

#colors <- c("green","darkblue","yellow", "red","blue") # Define the colors you're using

png(filename="~/Work/semestre_9/VD/projet_vd/salaire_region_2010.png")
par(mar=c(10.5, 4, 4, 2) + 0.1)
barplot(region[,1],beside=TRUE, col=c("red","blue"),las = 2,main="Salaire par region en 2010",ylab="salaire annuel moyen (en euro)")
dev.off()









